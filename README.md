# Phonebook API

Simple python solution to coding challenge on a hiring platform.

## Usage

If running for the first time, run `py initial_setup.py` first to create db file.
Then, run `py run.py` to start the server.

To run tests, run `pytest` in the base folder.

## API

#### Auth
To request a token required for authorization:
````sh
curl "http://localhost:5000/auth"
````
The response will contain a token to be included within the `Authorization` header.

#### Create
To create a phonebook entry:
````sh
curl -X POST -H 'Authorization:<token>' -H 'Content-Type:application/json' -d '{"first_name":"Jim","last_name":"Raynor","address":"NoValidationHere","phones":[{"number":"728363546", "type":1},{"number":"753774426","type":2}]}' "http://localhost:5000/phone"
````
Ids should not be included within the payload.

#### Read
To get a phonebook entry:
````sh
curl -H 'Authorization:<token>' "http://localhost:5000/phone/<id>"
````
To get all phonebook entries
````sh
curl -H 'Authorization:<token>'  "http://localhost:5000/phone"
````

#### Update
To update a phonebook entry, the id must be provided for the entry. If a phone number must be updated, the id must be provided, otherwise it will be assumed that it is a new phone number. If no ids are provided, or no fields are provided, response will be error code 400.
````sh
curl -X PUT -H 'Authorization:<token>' -H 'Content-Type:application/json' -d '{"id":1,"phones":[{"number":"728363546", "type":2}]}' "http://localhost:5000/phone"
````

#### Delete
To delete a phonebook entry:
````sh
curl -X DELETE -H 'Authorization:<token>' "http://localhost:5000/phone/<id>"
````
