import os
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(os.path.join(basedir, 'phonebook.db'))
SQLALCHEMY_TRACK_MODIFICATIONS = False