import os
import pytest
import json
from phonebook_api import create_app
from phonebook_api.models import db as _db, Person, Phone

basedir = os.path.abspath(os.path.dirname(__file__))

TESTDB_PATH = os.path.join(basedir, 'test_db.db')

@pytest.fixture(scope='session')
def app(request):
	test_settings = {
		'TESTING': True,
		'SQLALCHEMY_DATABASE_URI': 'sqlite:///{}'.format(TESTDB_PATH)
	}
	app = create_app(__name__, test_settings)

	ctx = app.app_context()
	ctx.push()

	def teardown():
		ctx.pop()

	request.addfinalizer(teardown)
	return app

@pytest.fixture(scope='session')
def db(app, request):
	if os.path.exists(TESTDB_PATH):
		os.remove(TESTDB_PATH)

	def teardown():
		_db.drop_all()
		os.unlink(TESTDB_PATH)

	_db.app = app
	_db.create_all()

	request.addfinalizer(teardown)
	return _db

@pytest.fixture(scope='function')
def session(db, request):
	con = db.engine.connect()
	txn = con.begin()

	options = {"bind":con, "binds":{}}
	session = db.create_scoped_session(options=options)

	db.session = session

	def teardown():
		txn.rollback()
		con.close()
		session.remove()

	request.addfinalizer(teardown)
	return session

class TestModel: 
	def test_person_model(self, session):
		person = Person(first_name='Sarah', last_name='Kerrigan')

		session.add(person)
		session.commit()

		assert person.id > 0

	def test_phone_model(self, session):
		person = Person(first_name='Sarah', last_name='Kerrigan')
		phone = Phone(number=12345678, type=3)
		person.phones.append(phone)

		session.add(person)
		session.commit()

		assert phone.id > 0

class TestAuth:
	def test_auth_code(self, app):
		with app.test_client() as c:
			r = c.get('/auth')
		assert r.status_code == 200

	def test_auth_reject(self, app):
		with app.test_client() as c:
			r = c.get('/phone')
		assert r.status_code == 401
		assert r.json['message'] == 'ERROR: Unauthorized.'

class TestGet:
	def test_get_one_not_exists(self, session, app):
		with app.test_client() as c:
			h = get_auth_header(c)
			r = c.get('/phone/0', headers=h)
		assert r.status_code == 404
		assert r.json['message'] == 'ERROR: Entry not found.'

	def test_get_one_exists(self, session, app):
		insert_test_data(session)
		with app.test_client() as c:
			h = get_auth_header(c)
			r = c.get('/phone/1', headers=h)
		assert r.status_code == 200
		assert r.json['id'] > 0

	def test_get_all_exists(self, session, app):
		insert_test_data(session)
		with app.test_client() as c:
			h = get_auth_header(c)
			r = c.get('/phone', headers=h)
		assert r.status_code == 200
		assert len(r.json) == 2

	def test_get_all_empty(self, session, app):
		with app.test_client() as c:
			h = get_auth_header(c)
			r = c.get('/phone', headers=h)
		assert r.status_code == 200
		assert len(r.json) == 0

class TestPost:
	def test_no_content_type(self, app):
		with app.test_client() as c:
			h = get_auth_header(c)
			r = c.post('/phone', headers=h)
		assert r.status_code == 400
		assert r.json['message'] == 'ERROR: Request requires json input.'

	def test_create_success(self, app, session):
		with app.test_client() as c:
			h = get_auth_header(c)
			add_content_type(h)
			r = c.post('/phone', headers=h,
					   data=json.dumps(
					   		{'address': 'Mar Sara',
					   		 'first_name': 'Tychus',
					   		 'last_name': 'Findlay',
					   		 'phones': [
					   		 	{'number': "12345",
					   		 	 'type': 2}]}))
		assert r.status_code == 201
		assert r.json['message'] == 'OK: Entry made.'
		assert r.json['data']['id'] > 0

	def test_bad_input(self, app, session):
		with app.test_client() as c:
			h = get_auth_header(c)
			add_content_type(h)
			r = c.post('/phone', headers=h,
					   data='thisisnotvalidjson')
		assert r.status_code == 400
		assert r.json['message'] == 'ERROR: Invalid data.'

class TestPut:
	def test_update_success(self, app, session):
		insert_test_data(session)
		with app.test_client() as c:
			h = get_auth_header(c)
			add_content_type(h)
			r = c.put('/phone', headers=h,
					  data=json.dumps(
					  	{'id':1,
					  	 'first_name': 'Queen of Blades'}))
		assert r.status_code == 200
		assert r.json['message'] == 'OK: Entry updated.'
		assert r.json['data']['first_name'] == 'Queen of Blades'

	def test_update_fail(self, app, session):
		with app.test_client() as c:
			h = get_auth_header(c)
			add_content_type(h)
			r = c.put('/phone', headers=h,
					  data=json.dumps(
					  	{'id':-1,
					  	 'first_name': 'Queen of Blades'}))
		assert r.status_code == 400
		assert r.json['message'] == 'ERROR: Invalid data.'

class TestDelete:
	def test_delete_success(self, app, session):
		insert_test_data(session)
		with app.test_client() as c:
			h = get_auth_header(c)
			add_content_type(h)
			r = c.delete('/phone/1', headers=h)
		assert r.status_code == 200
		assert r.json['message'] == 'OK: Entry deleted.'

	def test_delete_fail(self, app, session):
		with app.test_client() as c:
			h = get_auth_header(c)
			add_content_type(h)
			r = c.delete('/phone/1', headers=h)
		assert r.status_code == 404
		assert r.json['message'] == 'ERROR: Entry not found for deletion.'

def get_auth_header(client):
	resp = client.get('/auth')
	return {'Authorization': resp.json['token']}

def add_content_type(header={}):
	header['Content-Type'] = 'application/json'

def insert_test_data(ses):
	person = Person(first_name='Sarah', last_name='Kerrigan', address='Swarm')
	phone1 = Phone(number=12345678, type=2)
	phone2 = Phone(number=87654321, type=1)

	person.phones.append(phone1)
	person.phones.append(phone2)

	person2 = Person(first_name='Dark Prelate', last_name='Zeratul', address='Aiur')
	phone3 = Phone(number=18652506, type=1)

	person2.phones.append(phone3)

	ses.add(person)
	ses.add(person2)
	ses.commit()