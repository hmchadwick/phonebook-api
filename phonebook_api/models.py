from marshmallow import fields
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

db = SQLAlchemy()
ma = Marshmallow()

class Phone(db.Model):
	__tablename__ = 'phones'
	id = db.Column(db.Integer, 
				primary_key=True)
	person_id = db.Column(db.Integer, 
					   db.ForeignKey('person.id'), 
					   nullable=False)
	number = db.Column(db.String(20), 
					nullable=False)
	type = db.Column(db.Integer, 
				  nullable=False)

	def __repr__(self):
		return '<Phone # %r>' % self.number

class Person(db.Model):
	id = db.Column(db.Integer,
				primary_key=True)
	first_name = db.Column(db.String(50), 
						nullable=False)
	last_name = db.Column(db.String(50), 
					   nullable=False)
	address = db.Column(db.String(100))
	phones = db.relationship(Phone,
							 cascade='all, delete-orphan')

	def __repr__(self):
		return '<Person %r %r>' % (self.first_name, self.last_name)

class PhoneSchema(ma.ModelSchema):
	class Meta:
		model = Phone

class PersonSchema(ma.ModelSchema):
	phones = fields.Nested(PhoneSchema, many=True, exclude=('person_id',))
	class Meta:
		model = Person

person_schema = PersonSchema()
people_schema = PersonSchema(many=True)
phone_schema = PhoneSchema()