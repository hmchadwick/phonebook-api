from flask import Flask


def create_app(name=None, cfg=None):
	if not name:
		name = __name__
	app = Flask(name)

	app.config.from_object('config')

	if cfg is not None:
		app.config.update(**cfg)

	from phonebook_api.models import db, ma
	from phonebook_api.handlers import bp

	db.init_app(app)
	ma.init_app(app)

	app.register_blueprint(bp)

	return app