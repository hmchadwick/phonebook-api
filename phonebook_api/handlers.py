from flask import request, jsonify, Blueprint
from phonebook_api import create_app
from phonebook_api.models import db, Person, Phone, person_schema, people_schema, phone_schema
from itsdangerous import TimedSerializer, BadSignature, SignatureExpired

bp = Blueprint('phonebook_api', __name__)
s = TimedSerializer('SECRET')

@bp.route('/auth')
def generate_token():
	token = s.dumps(42)
	return jsonify({"token": token}), 200

@bp.route('/phone', methods=['POST'])
def create_person():
	try:
		person, _ = person_schema.load(request.json)
		db.session.add(person)
		db.session.commit()
		return jsonify({"message": "OK: Entry made.", "data": person_schema.dump(person).data}), 201
	except:
		return jsonify({"message": "ERROR: Invalid data."}), 400

@bp.route('/phone', methods=['PUT'])
def update_person():
	try:
		# get the person from db using id
		person = Person.query.get(request.json['id'])
		
		# make sure there is data to be updated
		updates = [x for x in request.json.keys() if x != 'id']
		if len(updates) == 0:
			raise

		for attr in updates:
			if attr == 'phones':
				continue
			setattr(person, attr, request.json[attr])

		if 'phones' in updates:
			# split list of phones into tobeupdated and new
			updated_phones = [x for x in request.json['phones'] if 'id' in x.keys()]
			new_phones = [x for x in request.json['phones'] if x not in updated_phones]

			for j_phone in updated_phones:
				# next() will cause update to fail if invalid phone id is included
				old_phone = next(p for p in person.phones if j_phone['id'] == p.id)
				for phone_attr in j_phone.keys():
					setattr(old_phone, phone_attr, j_phone[phone_attr])

			for j_phone in new_phones:
				phone, _ = phone_schema.load(j_phone)
				person.phones.append(phone)

		db.session.commit()
		return jsonify({"message": "OK: Entry updated.", "data": person_schema.dump(person).data}), 200
	except:
		return jsonify({"message": "ERROR: Invalid data."}), 400

@bp.route('/phone', methods=['GET'])
def read_person():
	all_people = Person.query.all()
	return people_schema.jsonify(all_people), 200

@bp.route('/phone/<person_id>', methods=['GET'])
def read_one_person(person_id):
	person = Person.query.get(person_id)
	if not person:
		return jsonify({"message": "ERROR: Entry not found."}), 404
	return person_schema.jsonify(person), 200


@bp.route('/phone/<person_id>', methods=['DELETE'])
def delete_person(person_id):
	person = Person.query.get(person_id)
	if not person:
		return jsonify({"message": "ERROR: Entry not found for deletion."}), 404
	db.session.delete(person)
	db.session.commit()
	return jsonify({"message": "OK: Entry deleted."}), 200

@bp.before_request
def check_token():
	try:
		if request.endpoint != 'phonebook_api.generate_token':
			auth = request.headers.get('Authorization')
			if not auth:
				raise BadSignature("")
			# tokens last for 10 minutes
			s.loads(auth, max_age=60000)

			if request.method in ("POST", "PUT") and not request.is_json:
				return jsonify({"message":"ERROR: Request requires json input."}), 400
	except SignatureExpired:
		return jsonify({"message": "ERROR: Token expired."}), 401
	except BadSignature:
		return jsonify({"message": "ERROR: Unauthorized."}), 401
